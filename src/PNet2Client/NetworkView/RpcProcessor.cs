using System;
using PNet;

namespace PNetC
{
    internal struct RpcProcessor
    {
        public readonly Action<NetMessage> Action;
        public readonly Func<NetMessage, object> Func;
        public readonly bool DefaultContinueForwarding;

        public RpcProcessor(Action<NetMessage> action, bool defaultContinueForwarding)
        {
            Action = action;
            DefaultContinueForwarding = defaultContinueForwarding;
            Func = null;
        }

        public RpcProcessor(Func<NetMessage, object> func, bool defaultContinueForwarding)
        {
            Action = null;
            DefaultContinueForwarding = defaultContinueForwarding;
            Func = func;
        }
    }
}